## Introduction

`htim` is a cross-platform interactive process viewer based on htop.

`htim` allows scrolling the list of processes vertically and horizontally to see their full command lines and related information like memory and CPU consumption.

The information displayed is configurable through a graphical setup and can be sorted and filtered interactively.

Tasks related to processes (e.g. killing and renicing) can be done without entering their PIDs.

Running `htim` requires `ncurses` libraries (typically named libncursesw*).

HTim, is htop improved, it has vim key bindings like h,j,k,l,g,G
and it also has a visual mode that can by used by pressing v

## Build instructions

This program is distributed as a standard GNU autotools-based package.

To compile first download the source from the Git the repository:
`git clone` or downloads from https://gitlab.com/thelinuxguy9/htim,
install the header files for `ncurses` (libncursesw*-dev) and other required development packages from your distribution's package manager. Then run:

~~~ shell
./autogen.sh && ./configure && make
~~~

By default `make install` will install into `/usr/local`, for changing the path use `./configure --prefix=/some/path`.

### Build Options

`htim` has several build-time options to enable/disable additional features.

#### Generic

  * `--enable-unicode`:
    enable Unicode support
    dependency: *libncursesw*
    default: *yes*
  * `--enable-hwloc`:
    enable hwloc support for CPU affinity; disables Linux affinity
    dependency: *libhwloc*
    default: *no*
  * `--enable-static`:
    build a static htim binary; hwloc and delay accounting are not supported
    default: *no*
  * `--enable-debug`:
    Enable asserts and internal sanity checks; implies a performance penalty
    default: *no*

#### Linux

  * `--enable-sensors`:
    enable libsensors(3) support for reading temperature data
    dependencies: *libsensors-dev*(build-time), at runtime *libsensors* is loaded via `dlopen(3)` if available
    default: *check*
  * `--enable-capabilities`:
    enable Linux capabilities support
    dependency: *libcap*
    default: *check*
  * `--with-proc`:
    location of a Linux-compatible proc filesystem
    default: */proc*
  * `--enable-openvz`:
    enable OpenVZ support
    default: *no*
  * `--enable-vserver`:
    enable VServer support
    default: *no*
  * `--enable-ancient-vserver`:
    enable ancient VServer support (implies `--enable-vserver`)
    default: *no*
   * `--enable-linux-affinity`:
    enable Linux `sched_setaffinity(2)` and `sched_getaffinity(2)` for affinity support; conflicts with hwloc
    default: *check*
  * `--enable-delayacct`:
    enable Linux delay accounting support
    dependencies: *pkg-config*(build-time), *libnl-3* and *libnl-genl-3*
    default: *check*

## Usage

See the on-line help ('F1' inside `htim`) for a list of supported key commands.

## Support, bugs, developent, and feedback

If you have any problem running the software, or any suggestion or requests,
and if you would like to support the software, see CONTRIBUTING.md.

## License

GNU General Public License, version 2 (GPL-2.0)
